//=========================================== tabs
$('.tabs-item').on('click', function () {
    $('.tabs-item').each(function (index, elem) {
        if (elem === event.target) {
            $(event.target).addClass('active');
            $('.tabs-content').eq(index).addClass('active');
        } else {
            $(elem).removeClass('active');
            $('.tabs-content').eq(index).removeClass('active');
        }
    })
})
//============================================== gallery-tabs
let countItems = 12;
$('.gallery-tabs-item').hide();
$('.gallery-tabs-item').each(function (index, item) {
    if (countItems > 0) {
        $(item).show();
        countItems = countItems - 1;
    }
});

$('.gallery-tabs-link').on('click', function () {
    $('#load-more').show('2000');
    $('.gallery-tabs-link').removeClass('active');
    $(event.target).addClass('active');
    let value = $(this).attr('data-filter');
    if (value === 'all') {

        let countItems = 12;
        //        $('.gallery-tabs-item').show('2000');
        $('.gallery-tabs-item').each(function (index, item) {
            if (countItems > 0) {
                $(item).show('2000');
                countItems = countItems - 1;
            } else {
                $(item).hide('2000');
            }
        });
    } else {
        $('.gallery-tabs-item').not('.' + value).hide('2000');
        $('.gallery-tabs-item').filter('.' + value).show('2000');
    }
})
$('#load-more').on('click', function () {
    let countItems = 24;
    $('#load-more').hide('2000');
    $('.gallery-tabs-item').each(function (index, item) {
        if (countItems > 0) {
            $(item).show('2000');
            countItems = countItems - 1;
        } else {
            $(item).hide('2000');
        }
    });
});
//=====================================================testimonials
$('.small-img-author').on('click', function () {
    $('.small-img-author').each(function (index, elem) {
        if (elem === event.target) {
            $('.testimonial-item').eq(index).addClass('active-text');
                $('.testimonial-author-wrapper-small').children().css({'border-color':'rgba(24, 207, 171, 0.3)'});
    $('.testimonial-author-wrapper-small.active-photo').children().css({'border-color':'#18cfab'});
        } else {
            $('.testimonial-author-wrapper-small').removeClass('active-photo');
            $(event.target).parent().addClass('active-photo');
            $('.testimonial-item').eq(index).removeClass('active-text');
                $('.testimonial-author-wrapper-small').children().css({'border-color':'rgba(24, 207, 171, 0.3)'});
    $('.testimonial-author-wrapper-small.active-photo').children().css({'border-color':'#18cfab'});
            
        }
    })
});

$('.testimonial-author-wrapper-small.active-photo').children().css({'border-color':'#18cfab'});

$('.arrow-next').on('click', function () {
    let curent = $('.testimonial-item.active-text');
    let curentIndex = $('.testimonial-item.active-text').index();
    let curentPhoto = $('.testimonial-author-wrapper-small.active-photo');
    if (curentIndex === $('.testimonial-item').length) {
        curentIndex = 0
     };
    curent.removeClass('active-text');
    curentPhoto.removeClass('active-photo');
    $('.testimonial-item').eq(curentIndex).addClass('active-text');
    $('.testimonial-author-wrapper-small').eq(curentIndex).addClass('active-photo');
    $('.testimonial-author-wrapper-small').children().css({'border-color':'rgba(24, 207, 171, 0.3)'});
    $('.testimonial-author-wrapper-small.active-photo').children().css({'border-color':'#18cfab'});
})
$('.arrow-previous').on('click', function () {
    let curent = $('.testimonial-item.active-text');
    let curentIndex = $('.testimonial-item.active-text').index();
    let curentPhoto = $('.testimonial-author-wrapper-small.active-photo');
    if (curentIndex === 1) {
        curentIndex = $('.testimonial-item').length-1;
    } else {
        curentIndex -= 2;
    }
    curent.removeClass('active-text');
    curentPhoto.removeClass('active-photo');
    $('.testimonial-item').eq(curentIndex).addClass('active-text');
    $('.testimonial-author-wrapper-small').eq(curentIndex).addClass('active-photo');
    $('.testimonial-author-wrapper-small').children().css({'border-color':'rgba(24, 207, 171, 0.3)'});
    $('.testimonial-author-wrapper-small.active-photo').children().css({'border-color':'#18cfab'});

       
})
//
//$('.grid').masonry({
//  itemSelector: '.grid-item',
//  columnWidth: 180,
//  isFitWidth: true
//});